<?php

// Error supressing and extend maximum execution time
//error_reporting(0);
ini_set('max_execution_time', 50000);

$script_start = microtime(TRUE);
echo 'Script Started - ' . $script_start . '<br/>';

flush();
ob_flush();

// Target site URL List
$cookies = array();
$site_url = array(
    'https://www.migamake.com/news.html'
);
$final_data = array();

for ($u = 0; $u < count($site_url); $u++) {
    list($site_source, $status_code) = curl_request($site_url[$u]);
    if ($status_code === 200) {
        // Ready data for parsing
        $document = new DOMDocument();
        $document->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $site_source);
        $xpath = new DOMXpath($document);

        $raw_post_count = $xpath->query('//div[@class="post"]');
        $post_count = $raw_post_count->length;

        for ($v = 0; $v < $post_count; $v++) { 
	        // Scraping Title
	        $raw_title = $xpath->query('//div[@class="post"]/h1/a');
	        $title = trim($raw_title->item($v)->nodeValue);

	        // Scraping Link
	        $raw_link = $xpath->query('//div[@class="post"]/h1/a/@href');
	        $link = 'https://www.migamake.com' . trim($raw_link->item($v)->nodeValue);

	        // Scraping Description
	        $raw_description = $xpath->query('//div[@class="post"]/p');
	        $description = trim($raw_description->item($p)->nodeValue);

	        // Scraping Date
	        $raw_date = $xpath->query('//div[@class="date"]');
	        $date = trim($raw_date->item($p)->nodeValue);

	        // Scraping Image
	        $raw_image = $xpath->query('//div[@class="image"]/a/img/@src');
	        $image = 'https://www.migamake.com/' . $raw_image->item($p)->nodeValue;

	        // Scraping Author
	        $raw_author = $xpath->query('//div[@class="author"]');
	        $author = trim($raw_author->item($p)->nodeValue);

	        array_push($final_data, array(
	            'title' => $title,
	            'link' => $link,
	            'description' => $description,
	            'img' => $image,
	            'date' => $date,
	            'author' => $author,
	        ));

	        echo '=====<br/>';
	        echo 'Title - ' . $title . '<br/>';
	        echo 'Link - ' . $link . '<br/>';

	        flush();
	        ob_flush();
        }
    }
}

// Converting data to XML
if (!empty($final_data)) {
    $activities = new SimpleXMLElement("<?xml version=\"1.0\"?><articles></articles>");
    array_to_xml($final_data, $activities);
    $xml_file = $activities->asXML('articles.xml');
} else {
    $activities = new SimpleXMLElement("<?xml version=\"1.0\"?><articles></articles>");
    $activities->addChild("error", htmlspecialchars("No content scraped from site. Stoping script."));
    $xml_file = $activities->asXML('activities.xml');
}

echo '=====<br/>';
if ($xml_file) {
    echo 'XML file have been generated successfully.<br/>';
} else {
    echo 'XML file generation error.<br/>';
}

$script_end = microtime(TRUE);
echo 'Script Ended - ' . $script_end . '<br/>';
$total_time = $script_end - $script_start;
$minutes = floor($total_time / 60);
$seconds = $total_time % 60;
echo 'Total time - ' . $minutes . ' Minutes ' . $seconds . ' Seconds<br/>';

// CURL request Function
function curl_request($url = '') {
    global $cookies;
    $source_search = array('&nbsp;');
    $source_replace = array(' ');
    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
            "cache-control: no-cache",
            "upgrade-insecure-requests: 1",
            "user-agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"
        ),
    ));
    // Parsing Cookie from the response header
    curl_setopt($ch, CURLOPT_HEADERFUNCTION, "curl_response_header_callback");
    $url_source = str_replace($source_search, $source_replace, curl_exec($ch));
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return array($url_source, $status_code);
}

// Cookie Parsing Function
function curl_response_header_callback($ch, $headerLine) {
    global $cookies;
    if (preg_match('/^Set-Cookie:\s*([^;]+)/mi', $headerLine, $match) === 1) {
        if (false !== ($p = strpos($match[1], '='))) {
            $replaced = false;
            $cname = substr($match[1], 0, $p + 1);
            foreach ($cookies as &$cookie) {
                if (0 === strpos($cookie, $cname)) {
                    $cookie = $match[1];
                    $replaced = true;
                    break;
                }
            }
            if (!$replaced) {
                $cookies[] = $match[1];
            }
        }
    }
    return strlen($headerLine);
}

// Recursive Function for creating XML Nodes
function array_to_xml($array, &$activities) {
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            if (!is_numeric($key)) {
                $subnode = $activities->addChild("$key");
                array_to_xml($value, $subnode);
            } else {
                $subnode = $activities->addChild("post");
                array_to_xml($value, $subnode);
            }
        } else {
            $activities->addChild("$key", htmlspecialchars("$value"));
        }
    }
}

function check_data($value = '') {
    if (!empty($value)) {
        echo '<pre>';
        var_dump($value);
        echo '</pre>';
    }
    die('Script execution stopped');
}